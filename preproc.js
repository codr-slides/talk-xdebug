// Based on https://github.com/webpro/reveal-md/issues/228

const { readFileSync, readdirSync } = require('fs');

const LINE_SEPARATOR = '\n';
const SLIDE_SEPARATOR = '\n----\n';
const FILE_REF_REGEX = /^FILE: (.+)$/;
const DIR_REF_REGEX = /^DIR: (.+)$/;

const NUM_TITLE_REGEX = /^(.+) _NUM_TITLE_ (.+)$/;
const SUB_NUM_TITLE_REGEX = /^(.+) _SUB_NUM_TITLE_ (.+)$/;
const SET_NUM_REGEX = /^_SET_NUM_(X|Y|Z|A)__(.+)$/;

const isFileReference = line => FILE_REF_REGEX.test(line);
const isDirReference = line => DIR_REF_REGEX.test(line);
const hasSetNum = line => SET_NUM_REGEX.test(line);
const hasNumTitle = line => NUM_TITLE_REGEX.test(line);
const hasSubNumTitle = line => SUB_NUM_TITLE_REGEX.test(line);

const currentNum = { "X": 0, "Y": 0, "Z": 0, "A": 0 }

const loadDirContent = line => {
    const directory = line.match(DIR_REF_REGEX)[1];
    return readdirSync(directory)
        .filter(file => /\.md$/.test(file))
        .map(file => includeFiles(readFileSync(`${directory}/${file}`, 'utf8')))
        .join(SLIDE_SEPARATOR);
};

const loadFileContent = line => {
    const filePath = line.match(FILE_REF_REGEX)[1];
    return includeFiles(readFileSync(filePath, 'utf8'));
};

const injectNumTitle = line => {
    currentNum.A = 0;
    currentNum.Z++;

    const parts = line.match(NUM_TITLE_REGEX);
    const num = [currentNum.X, currentNum.Y, currentNum.Z].join('.');

    return `${parts[1]} ${num} ${parts[2]}`;
};

const injectSubNumTitle = line => {
    currentNum.A++;

    const parts = line.match(SUB_NUM_TITLE_REGEX);
    const num = [currentNum.X, currentNum.Y, currentNum.Z, currentNum.A].join('.');

    return `${parts[1]} ${num} ${parts[2]}`;
};

const setNum = line => {
    const parts = line.match(SET_NUM_REGEX);
    currentNum[parts[1]] = parseInt(parts[2]);
};

const includeFiles = markdown => {
    return markdown
        .split(LINE_SEPARATOR)
        .map(line => {
            if (isFileReference(line)) {
                return loadFileContent(line);
            }
            if (isDirReference(line)) {
                return loadDirContent(line);
            }
            if (hasNumTitle(line)) {
                return injectNumTitle(line)
            }
            if (hasSubNumTitle(line)) {
                return injectSubNumTitle(line)
            }
            if (hasSetNum(line)) {
                return setNum(line)
            }
            return line;
        })
        .join(LINE_SEPARATOR);
}

module.exports = async markdown => includeFiles(markdown);