<?php

use App\Loop\ItemRepository;
use App\Loop\ItemRepositoryBug;

require_once 'vendor/autoload.php';

$repository = new ItemRepository();
//$repository = new ItemRepositoryBug();

$items = $repository->getItems();

foreach($items as $item) {
    $ancestors = $repository->findItemAncestors($item);

    echo $item->name;

    foreach($ancestors as $ancestor) {
        echo ' < ' . $ancestor->name;
    }

    echo '<br />';
}
