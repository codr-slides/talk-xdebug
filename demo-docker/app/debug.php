<?php

require_once 'vendor/autoload.php';

function checkPasswordStrength(string $value): string {
    if(1 === preg_match('/^[a-z]+$/', $value)) {
      return 'Cette chaine n\'est pas valide.';
    }

    return 'Cette chaine est valide.';
}

$values = [
  'bAr',
  'bar',
];

foreach($values as $value) {
    $check = checkPasswordStrength($value);

    dump($value);
    dump($check);
    dump('-----');
}

