<?php

require_once 'vendor/autoload.php';

use App\Commerce\Order;
use App\Commerce\Product;

// Fetch data
$products = [
    new Product("Product 1", 100, 100),
    new Product("Product 2", 100, 90),
//    new Product("Product 3", 100, 100),
//    new Product("Product 4", 100, 100),
//    new Product("Product 5", 100, 90),
//    new Product("Product 6", 100, 90),
//    new Product("Product 7", 100, 100),
//    new Product("Product 8", 100, 100),
//    new Product("Product 9", 100, 90),
//    new Product("Product 10", 100, 100),
//    new Product("Product 11", 100, 100),
//    new Product("Product 12", 100, 100),
//    new Product("Product 13", 100, 100),
//    new Product("Product 14", 100, 100),
//    new Product("Product 15", 100, 90),
//    new Product("Product 16", 100, 90),
//    new Product("Product 17", 100, 100),
//    new Product("Product 18", 100, 100),
//    new Product("Product 19", 100, 90),
];

// Create my order with products and promotions
$order = new Order();

foreach ($products as $product) {
    $order->addProduct($product);
}

// Display Cart
echo "<h1>Panier</h1>";

foreach($order->getProducts() as $product) {
    echo "<h3>" . $product->name . " -> " . $product->showPrice() . "</h3>";
}

echo "<h2>Montant du panier " . $order->getRawAmount() . "</h2>";

echo "<h2>Vos points avantage</h2> (un point par produit achete au prix fort)";

echo '<h3>';

for($i=0; $i < $order->countProductsWithReducedPrice(); $i++) {
    echo '*';
}
echo '</h3>';