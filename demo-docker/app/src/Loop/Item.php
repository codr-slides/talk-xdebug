<?php

namespace App\Loop;

class Item {
    public function __construct(readonly int $id, readonly string $name, readonly ?int $parent)
    {
    }
}
