<?php

namespace App\Loop;

class ItemRepositoryBug {

    /**
     * @var Item[]
     */
    private array $items;

    public function __construct()
    {
        $fixtures = [
            [
                'id' => 1,
                'name' => 'Root',
                'parent' => null,
            ],
            [
                'id' => 2,
                'name' => 'Foo',
                'parent' => 1,
            ],
            [
                'id' => 3,
                'name' => 'Bar',
                'parent' => 2,
            ],
            [
                'id' => 4,
                'name' => 'Oeuf',
                'parent' => 5,
            ],
            [
                'id' => 5,
                'name' => 'Poule',
                'parent' => 4,
            ],
        ];

        $this->items = [];
        foreach($fixtures as $fixture) {
            $this->items[] = new Item($fixture['id'], $fixture['name'], $fixture['parent']);
        }
    }

    function findItemById(int $id): ?Item {
        foreach ($this->items as $item) {
            if($item->id === $id) {
                return $item;
            }
        }

        return null;
    }

    function findItemAncestor(Item $item): ?Item {
        if($item->parent !== null) {
            return $this->findItemById($item->parent);
        }

        return null;
    }

    /**
     * @return Item[]
     */
    function findItemAncestors(Item $item, array $ancestors = []): array {
        $parent = $this->findItemAncestor($item);

        if(!$parent) {
            return $ancestors;
        }

        return $this->findItemAncestors($parent, array_merge($ancestors, [$parent]));
    }

    public function getItems(): array
    {
        return $this->items;
    }
}
