<?php

namespace App\Commerce;

readonly class Promotion {

    public function __construct(
        public PromotionType $type,
        public float $value,
        public int $weight = 1,
    ) {}
}