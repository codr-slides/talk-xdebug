<?php

namespace App\Commerce;

class Order {
    /** @var Product[] */
    private array $products;

    /** @var Promotion[] */
    private array $promotions;

    public function __construct() {
        $this->products = [];
        $this->promotions = [];
    }

    public function addProduct(Product $product): void {
        $this->products[] = $product;
    }

    public function addPromotion(Promotion $promotion): void {
        $this->promotions[] = $promotion;
    }

    public function getRawAmount(): float {
        // Get total price
        return array_reduce($this->products, function($amount, Product $product){
            return $amount + $product->sellingPrice;
        }, 0);
    }

    public function getAmount(): float {
        // Sort promotions by weight
        usort($this->promotions, function(Promotion $a, Promotion $b){
           return $a->weight >= $b->weight ? 1 : -1;
        });

        // Subtract promotions
        return array_reduce($this->promotions, function($amount, Promotion $promotion){
            return match ($promotion->type) {
                PromotionType::FixedAmount => $amount - $promotion->value,
                PromotionType::Percentage => $amount - ($amount * $promotion->value / 100),
            };
        }, $this->getRawAmount());
    }

    public function getProducts(): array
    {
        return $this->products;
    }

    public function countProductsWithReducedPrice(): int
    {
        // Volontary slow down
        /** @noinspection PhpStatementHasEmptyBodyInspection */
        for ($i = 0; $i < 10000000; $i++) {}

        return count(array_filter($this->products, function($product){
            return $product->catalogPrice === $product->sellingPrice;
        }));
    }

    public function getPromotions(): array
    {
        return $this->promotions;
    }
}