<?php

namespace App\Commerce;

enum PromotionType
{
    case FixedAmount;
    case Percentage;
}