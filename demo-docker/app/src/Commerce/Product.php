<?php

namespace App\Commerce;

readonly class Product {
    public function __construct(
        public string $name,
        public float $catalogPrice,
        public float $sellingPrice
    ) { }

    public function showPrice(): string {
        if($this->sellingPrice === $this->catalogPrice) {
            return $this->sellingPrice.'€';
        }

        return '<span style="text-decoration: line-through">' . $this->catalogPrice . '€</span> ' . $this->sellingPrice.'€';
    }
}