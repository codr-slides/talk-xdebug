<?php

namespace App;

use App\Commerce\Product;
use App\Commerce\Promotion;
use App\Commerce\PromotionType;

class Repository {
    /**
     * @return Product[]
     */
    public function findProducts(): array
    {
        return [
            new Product("My demo product 1", 100, 100),
            new Product("My demo product 2", 100, 100),
        ];
    }

    /**
     * @return Promotion[]
     */
    public function findPromotions(): array
    {
        return [
            new Promotion(PromotionType::FixedAmount, 50),
            new Promotion(PromotionType::Percentage, 10),
        ];
    }
}