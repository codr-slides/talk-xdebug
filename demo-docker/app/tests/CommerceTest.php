<?php declare(strict_types=1);

namespace Test;

use App\Commerce\Order;
use App\Commerce\Product;
use App\Commerce\Promotion;
use App\Commerce\PromotionType;
use PHPUnit\Framework\TestCase;

final class CommerceTest extends TestCase
{
    public function testProcessSingleProduct(): void
    {
        $product1 = new Product("Foo Product", 100, 100);
        $order = new Order();
        $order->addProduct($product1);
        $this->assertSame(100.0, $order->getAmount());
    }

    public function testProcessMultipleProduct(): void
    {
        $product1 = new Product("Foo Product", 100, 100);
        $product2 = new Product("Bar Product", 10, 10);
        $product3 = new Product("Baz Product", 5, 5);

        $order = new Order();
        $order->addProduct($product1);
        $order->addProduct($product2);
        $order->addProduct($product3);

        $this->assertSame(115.0, $order->getAmount());
    }

    public function testProcessMultipleProductAlteredPrice(): void
    {
        $product1 = new Product("Foo Product", 100, 50);
        $product2 = new Product("Bar Product", 10, 10);
        $product3 = new Product("Baz Product", 5, 5);

        $order = new Order();
        $order->addProduct($product1);
        $order->addProduct($product2);
        $order->addProduct($product3);

        $this->assertSame(65.0, $order->getAmount());
    }

    public function testFixedPromotions(): void
    {
        $product1 = new Product("Foo Product", 100, 100);

        $promotion1 = new Promotion(PromotionType::FixedAmount, 10);
        $promotion2 = new Promotion(PromotionType::FixedAmount, 5);

        $order = new Order();

        $order->addProduct($product1);

        $order->addPromotion($promotion1);
        $order->addPromotion($promotion2);

        $this->assertSame(85.0, $order->getAmount());
    }

    public function testPercentPromotions(): void
    {
        $product1 = new Product("Foo Product", 100, 100);

        $promotion1 = new Promotion(PromotionType::Percentage, 10, 1);
        $promotion2 = new Promotion(PromotionType::Percentage, 5, 2);

        $order = new Order();

        $order->addProduct($product1);

        $order->addPromotion($promotion1);
        $order->addPromotion($promotion2);

        $this->assertSame(85.5, $order->getAmount());
    }

    public function testMixedPromotion(): void
    {
        $product1 = new Product("Foo Product", 100, 100);

        $promotion1 = new Promotion(PromotionType::Percentage, 10, 1);
        $promotion2 = new Promotion(PromotionType::FixedAmount, 10, 2);

        $order = new Order();

        $order->addProduct($product1);

        $order->addPromotion($promotion1);
        $order->addPromotion($promotion2);

        $this->assertSame(80.0, $order->getAmount());
    }

    public function testMixedPromotionReverse(): void
    {
        $product1 = new Product("Foo Product", 100, 100);

        $promotion1 = new Promotion(PromotionType::FixedAmount, 10, 1);
        $promotion2 = new Promotion(PromotionType::Percentage, 10, 2);

        $order = new Order();

        $order->addProduct($product1);

        $order->addPromotion($promotion1);
        $order->addPromotion($promotion2);

        $this->assertSame(81.0, $order->getAmount());
    }
}
