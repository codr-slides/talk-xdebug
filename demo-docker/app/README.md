# index.php

Utilisation de xdebug

* Poser un point d'arrêt
* Lire l'état des variables
* Modifier l'état d'une variable

# debug.php

Utilisation du pas à pas

* Step over
* Step into
    * Dans le fichier
    * Dans un autre fichier (ex: vendor)

# panier.php

Débogage d'une lib externe

* Ajout de produits au panier
* Ajout de promotions
* Le calcul de la promotion n'est pas correct
* La lib est testée unitairement
* Aucun message d'erreur

# loop.php

* Dépassement de mémoire dans un cas d'une fonction recursive

# bad_loop.php

* Une boucle mal implémentée passe le code review (oops)
* Avec 2 produits, ça semble ok.
* Avec 20 produits, ça commence à prendre du temps.