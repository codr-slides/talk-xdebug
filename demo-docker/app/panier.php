<?php

require_once 'vendor/autoload.php';

use App\Commerce\Order;
use App\Repository;

// Fetch data from "db"
$repository = new Repository();
$products = $repository->findProducts();
$promotions = $repository->findPromotions();

// Create my order with products and promotions
$order = new Order();

foreach ($products as $product) {
    $order->addProduct($product);
}

foreach ($promotions as $promotion) {
    $order->addPromotion($promotion);
}

// Display Cart
echo "<h1>Panier</h1>";

foreach($order->getProducts() as $product) {
    echo "<h3>" . $product->name . " -> " . $product->showPrice() . "</h3>";
}

echo "<h2>Montant du panier " . $order->getRawAmount() . "</h2>";

foreach($order->getPromotions() as $promotion) {
    echo "<h3>Promotion: " . $promotion->type->name . " (" . $promotion->value . ")</h3>";
}

echo "<h2>Montant total " . $order->getAmount() . "</h2>";

/*
Montant 200

Promotion fixe - 50 => 150
Promotion pourcentage - 10% => 150 - 15 => 135

Affiché sur le site => 130
*/