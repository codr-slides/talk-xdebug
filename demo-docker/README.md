# FROM official php image

```shell
cd php

docker build -t xdebug/php .
docker run -ti -v "$PWD/../app":/var/www/html -p "8000:80" --add-host host.docker.internal:host-gateway xdebug/php
```

http://localhost:8000/

# FROM wodby/php

```shell
cd wodby

docker compose up
```

http://localhost:8000/

Enter in container php

```shell
docker exec -ti xdebug_php bash
```