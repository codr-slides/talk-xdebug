FROM node:21-alpine AS base

# Install dependencies only when needed
FROM base AS deps
# Check https://github.com/nodejs/docker-node/tree/b4117f9333da4138b03a546ec926ef50a31506c3#nodealpine to understand why libc6-compat might be needed.
RUN apk add --no-cache libc6-compat
WORKDIR /app

# Install dependencies based on the preferred package manager
COPY package.json yarn.lock* package-lock.json* pnpm-lock.yaml* ./
RUN \
  if [ -f yarn.lock ]; then yarn --frozen-lockfile; \
  elif [ -f package-lock.json ]; then npm ci; \
  elif [ -f pnpm-lock.yaml ]; then yarn global add pnpm && pnpm i --frozen-lockfile; \
  else echo "Lockfile not found." && exit 1; \
  fi


# Rebuild the source code only when needed
FROM base AS builder
WORKDIR /app
COPY --from=deps /app/node_modules ./node_modules
COPY . .

# Remove "thanks", and "myself" slides
RUN rm /app/slides/intro/2-thanks.md
RUN rm /app/slides/intro/3-myself.md

# If using npm comment out above and use below instead
RUN npm run build-themes
RUN npm run build-slides

# Build small image to serv static
FROM nginx:1.25-alpine

COPY --from=builder /app/slides/assets /usr/share/nginx/html/assets
COPY --from=builder /app/build /usr/share/nginx/html


