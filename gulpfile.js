const dir = './css/theme/source/*.{sass,scss}'

const gulp = require('gulp')
const shell = require('gulp-shell')
const sass = require('gulp-dart-sass');
const mode = require('gulp-mode')({
  modes: ['production', 'development'],
  default: 'development',
  verbose: false
});

const sourcemaps = require('gulp-sourcemaps');

const sassOptions = {
  outputStyle: (mode.production() ? 'compressed' : 'expanded')
};

gulp.task('themes:build', () => {
  return gulp.src(dir)
    .pipe((mode.development(sourcemaps.init({}))))
    .pipe(sass.sync(sassOptions).on('error', sass.logError))
    .pipe((mode.development(sourcemaps.write('.'))))
    .pipe(gulp.dest('./dist/theme'))
})

gulp.task('themes:watch', () => gulp.watch(dir, {"queue": true}, gulp.series('themes:build')))
