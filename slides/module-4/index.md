_SET_NUM_X__4_
_SET_NUM_Y__1_
_SET_NUM_Z__0_
_SET_NUM_A__0_

# Pas à Pas

![alt text](./assets/_gifs/spongebob/lost.gif)

----

## _NUM_TITLE_ / Navigation dans le code

![alt text](./assets/_slides/module-4/Screenshot_20240507_224936.png)

* Step over: Execute la ligne actuelle
* Step into: Rentre dans la fonction appelée (le cas échéant)
    * Dans ce fichier ou dans un autre

---

_SET_NUM_X__4_
_SET_NUM_Y__2_
_SET_NUM_Z__0_
_SET_NUM_A__0_

# Profiling

![alt text](./assets/_gifs/spongebob/fire.gif)

----

## _NUM_TITLE_ / Activation du mode profile

```
xdebug.mode="profile"
xdebug.mode="debug, profile"
```

* Chaque thread php va générer un fichier
* On peut le consulter avec des outils comme webgrind

----

## _NUM_TITLE_ / Webgrind / Tableau

![alt text](./assets/_slides/module-4/Screenshot_20240507_231250.png)

----

## _NUM_TITLE_ / Webgrind / Graph

![alt text](../assets/_slides/module-4/Screenshot_20240507_231727.png)

----

## _NUM_TITLE_ / Vitesse du ClassLoader.php

```
composer install
```

![alt text](../assets/_slides/module-4/Screenshot_20240507_231826.png)

----

## _NUM_TITLE_ / Vitesse du ClassLoader.php

```
composer install -o
```

![alt text](../assets/_slides/module-4/Screenshot_20240507_232016.png)

----

## _NUM_TITLE_ / Disclaimer

* Ne pas utiliser xdebug en prod
* Ne pas utiliser xdebug en prod
* Ne pas utiliser xdebug en prod
* Pour une utilisation en prod: 
    * [xhprof](https://pecl.php.net/package/xhprof) by Facebook
    * [blackfire](https://www.blackfire.io/) by Platform.sh

Note: 

* Encore une fois, pas un bug de l'ecrire 3 fois.
* Démonstration live

---

# Démonstration live

![alt text](./assets/_gifs/spongebob/watch.gif)

----

![alt text](./assets/_gifs/spongebob/flex.gif)