_SET_NUM_X__1_
_SET_NUM_Y__1_
_SET_NUM_Z__0_
_SET_NUM_A__0_


![alt text](./assets/_gifs/spongebob/hands.gif)

Note:

* Qui fait du php ?
* Qui a entendu parler de xdebug ?
* Qui a déja utilisé ou essayé (une fois au moins) ?
* Qui a déja eu des difficultés a mettre en place ?

----

## La première fois qu'on essaye d'installer XDEBUG

![alt text](./assets/_gifs/spongebob/angry2.gif)

Note: 

* Comme phpunit, la premiere fois c'est toujours compliqué
* Mais avant d'installer, on va voir ce que c'est pour les quelques uns qui ne connaissent pas encore.

----

## _NUM_TITLE_ / C'est quoi ?

* Une extension de php (cross platform)
* Outil de debuggage
* Met le code en pause
* Permet de connaitre l'état de la mémoire à chaque instant
* Possibilité de modifier une variable à la volée
* Qui peut profiler une application
* Et bien plus

----

## _NUM_TITLE_ / Pour qui ?

* Développeurs/euses PHP
* Quelque soit le projet
* Quelque soit le framework
* Quelque soit la version de PHP
* Quelque soit votre niveau

Note: 

* Pas un outil réservé aux experts.
* Tout le monde peut l'utiliser

----

## _NUM_TITLE_ / Pourquoi ?

* Isoler une ligne / une condition qui bug
* Comprendre le fonctionnement d'un algo
* Étudier le fonctionnement d'un vendor
* Comprendre une lenteur
* Tester une implémentation (rapidité par exemple)

----

## _NUM_TITLE_ / Cependant

* Ne pas utiliser en prod
* Ne pas utiliser en prod
* Ne pas utiliser en prod

Note:

Ce n'est pas un bug, j'ai volontairement ecrit 3 fois pour être sur que statistiquement vous lisiez au moins une des lignes.
