---
title: XDEBUG, c'est cool ! | Guillaume Aveline
revealOptions:
  width: 1920
  height: 1080
  # Factor of the display size that should remain empty around the content
  margin: 0.14
  # Bounds for smallest/largest possible scale to apply to content
  minScale: 0.2
  maxScale: 2.0
  hashOneBasedIndex: false
  # showNotes: true
  # previewLinks: true
  # viewDistance: 6
  # transition: slide
  autoPlayMedia: true
  center: true
  markdown:
    breaks: true
    smartypants: true
---

DIR: ./slides/intro

---

FILE: ./slides/module-1/index.md

---

FILE: ./slides/module-2/index.md

---

FILE: ./slides/module-3/index.md

---

FILE: ./slides/module-4/index.md

---

FILE: ./slides/module-5/index.md

---

DIR: ./slides/outro
