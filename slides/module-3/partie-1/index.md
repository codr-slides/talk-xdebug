_SET_NUM_X__3_
_SET_NUM_Y__1_
_SET_NUM_Z__0_
_SET_NUM_A__0_

# VSCode

----

## _NUM_TITLE_ / Extension

* Installer l'extension 'php debug' (xdebug.php-debug)

![alt text](./assets/_slides/module-3/Screenshot_20240425_151311.png)

----

## _NUM_TITLE_ / Configuration

* Ajouter les configurations de debug fournies par l'extension

![alt text](./assets/_slides/module-3/Screenshot_20240425_151706.png)
![alt text](./assets/_slides/module-3/Screenshot_20240425_151723.png)

----

## _NUM_TITLE_ / Écouter xdebug

* Aller dans Run & Debug
* "Listen for Xdebug"

![alt text](./assets/_slides/module-3/Screenshot_20240425_151816.png)

----

## _NUM_TITLE_ / Débugger

* Mettre un point d'arrêt (click à gauche du numéro de ligne)

![alt text](./assets/_gifs/spongebob/perfect.gif)

----

<video controls>
  <source src="./assets/_slides/module-3/vscode.mp4" type="video/mp4">
</video>

----

## _NUM_TITLE_ / Spécificité docker

* Ajout du mapping (dossier hôte <-> dossier container)
* Dans le fichier de configuration du projet `.vscode/launch.json`

```json

{
  "configurations": [
    {
      //...
      "pathMappings": {
        "/var/www/html": "${workspaceRoot}"
      }
    },
  ]
}
```
