_SET_NUM_X__3_
_SET_NUM_Y__2_
_SET_NUM_Z__0_
_SET_NUM_A__0_

# PHPStorm

----

## _NUM_TITLE_ / Configuration

![alt text](./assets/_gifs/spongebob/magical.gif)

* Pas de configuration supplémentaire

Note: 

* (avec nos exemples)
* Attention si les variables sont configurés: 
    * env `IDE_CONFIG`
    * `xdebug.idekey`

----

## _NUM_TITLE_ / Spécificité docker

* Ajout du mapping (dossier hôte <-> dossier container)

Note: 

* Peut se faire automatiquement.
