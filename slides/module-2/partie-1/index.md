_SET_NUM_X__2_
_SET_NUM_Y__1_
_SET_NUM_Z__0_
_SET_NUM_A__0_

# Installation
## Linux

----

![alt text](./assets/_gifs/spongebob/anxious.webp)

Note:
* Ne vous inquietez pas, tout va bien se passer.

----

## _NUM_TITLE_ / Mise en place du serveur

Note: 

* On passe rapidement sur cette premiere etape
* Install nginx et php-fpm classique

----

## _SUB_NUM_TITLE_ / Installation Nginx/PHP

```shell
apt update -y
apt upgrade -y
apt install nginx php8.1-fpm -y
```

```shell
systemctl status nginx
systemctl status php8.1-fpm
```

----

## _SUB_NUM_TITLE_ / Configuration Nginx

```shell
nano /etc/nginx/sites-available/default
```

```nginx
server {
  # Example PHP Nginx FPM config file
  listen 80 default_server;
  listen [::]:80 default_server;
  root /var/www/html;

  # Add index.php to setup Nginx, PHP & PHP-FPM config
  index index.php index.html index.htm index.nginx-debian.html;

  server_name _;

  location / {
    try_files $uri $uri/ =404;
  }

  # pass PHP scripts on Nginx to FastCGI (PHP-FPM) server
  location ~ \.php$ {
    include snippets/fastcgi-php.conf;

    # Nginx php-fpm sock config:
    fastcgi_pass unix:/run/php/php8.1-fpm.sock;
    # Nginx php-cgi config :
    # Nginx PHP fastcgi_pass 127.0.0.1:9000;
  }

  # deny access to Apache .htaccess on Nginx with PHP, 
  # if Apache and Nginx document roots concur
  location ~ /\.ht {
    deny all;
  }
} # End of PHP FPM Nginx config example
```

Note: 
 * On va juste mettre php fpm, rien de spécifique ici.
 * Pour faire du https, rien de spécifique en plus de rajouter une ecoute sur 443 et les certificats TLS

----

## _SUB_NUM_TITLE_ / Configuration Nginx

```
nginx -t
systemctl restart nginx
```

Note: 

```
chown -R guillaume:guillaume /var/www/html
```

----

## _SUB_NUM_TITLE_ / Configuration PHP

```shell
cd ~
mkdir www
echo "<?php phpinfo();" >> ~/www/index.php
```

----

## _NUM_TITLE_ / XDebug

----

## _SUB_NUM_TITLE_ / Installation

```
sudo apt install php8.1-xdebug
```

Note: 

* Sur debian avec uniquement php 8.1
* Valables avec toutes les versions

----

## _SUB_NUM_TITLE_ / Vérification

```php
phpinfo();
```

```php
xdebug_info()
```

```shell
php -i | grep xdebug
```

----

## _SUB_NUM_TITLE_ / Configuration de base

```shell
nano /etc/php/8.1/fpm/conf.d/20-xdebug.ini
```

```ini
[xdebug]
zend_extension=xdebug.so
xdebug.mode=debug
xdebug.start_with_request=yes
```

```shell
systemctl restart php8.1-fpm
```

----

<video controls>
  <source src="./assets/_slides/module-2/xdebug.mp4" type="video/mp4">
</video>

Note: 

* virtualbox
  * from scratch
  * vscode xdebug
  * install nginx, php
  * config
  * intall xdebug
  * test

----

![alt text](./assets/_gifs/spongebob/thumbsup3.gif)

----

## _SUB_NUM_TITLE_ / RTFM

Pour aller plus loin: 

* [https://xdebug.org/docs/all_settings](https://xdebug.org/docs/all_settings)

Note: 

* En toute amitié.
* Plein de config possibles
* Du log, etc...