_SET_NUM_X__2_
_SET_NUM_Y__2_
_SET_NUM_Z__0_
_SET_NUM_A__0_

# Installation
## Docker

----

![alt text](./assets/_gifs/spongebob/anxious.gif)

* Plusieurs façons d'utiliser docker <!-- .element: class="fragment fade-left" -->
* Plus compliqué que sur ma machine <!-- .element: class="fragment fade-left" -->
* On est pas vraiment en local <!-- .element: class="fragment fade-left" -->
* ...  <!-- .element: class="fragment fade-left" -->

----

## _NUM_TITLE_ / FROM php

* [Image officielle php](https://hub.docker.com/_/php)

----

## _SUB_NUM_TITLE_ / Dockerfile

```dockerfile[1|3-4|6]
FROM php:8.3-apache

RUN pecl install xdebug \
	&& docker-php-ext-enable xdebug

COPY ./99-xdebug.ini "$PHP_INI_DIR/conf.d/"  
```

Note: 

* Prévoir le fichier `99-xdebug.ini` dans notre arbo de projet.
* Il sera ainsi copié dans notre conteneur.

----

## _SUB_NUM_TITLE_ / 99-xdebug.ini

```ini [5]
[xdebug]
xdebug.mode=debug
xdebug.start_with_request=yes
xdebug.log_level=0
xdebug.client_host=host.docker.internal
```

----

## _SUB_NUM_TITLE_ / Utilisation

```shell
docker build -t xdebug/php .
docker run -ti -v "$PWD/../app":/var/www/html -p "8000:80" xdebug/php
```

```shell
# Pour linux
docker run -ti -v "$PWD/../app":/var/www/html -p "8000:80" --add-host host.docker.internal:host-gateway xdebug/php
```
<!-- .element: class="fragment fade-left" -->

----

<video controls>
  <source src="./assets/_slides/module-2/docker_php.mp4" type="video/mp4">
</video>

----

## _NUM_TITLE_ / FROM wodby/php

Série d'images docker pour simplifier la mise en place d'une stack.

* [https://github.com/wodby](https://github.com/wodby)

----

## _SUB_NUM_TITLE_ / docker-compose.yml

```dockerfile[1-12|14-25]
services:
  nginx:
    image: wodby/nginx
    container_name: "xdebug_nginx"
    depends_on:
      - php
    ports:
      - "8000:80"      
    environment:
       #...
    volumes:
      - ../app:/var/www/html:cached

  php:
    image: wodby/php:8.3-dev
    container_name: "xdebug_php"
    environment:
      PHP_EXTENSIONS_DISABLE: "xhprof,spx"
      PHP_XDEBUG_LOG_LEVEL: "0"
      PHP_XDEBUG_START_WITH_REQUEST: "yes"
      PHP_XDEBUG_MODE: "debug"
    extra_hosts:
      - "host.docker.internal=host-gateway"  
    volumes:
      - ../app:/var/www/html:cached
```

Note:

* Par defaut, l'image php de wodby désactive les extensions
  * xdebug
  * xhprof (profiler créé par facebook)
  * spx (profiler Simple Profiling eXtension)
* donc on surcharge les extension désactivées pour retirer xdebug de la liste.

----

## _SUB_NUM_TITLE_ / Utilisation

```shell
docker compose up
```

----

<video controls>
  <source src="./assets/_slides/module-2/docker_wodby.mp4" type="video/mp4">
</video>

----

![alt text](./assets/_gifs/spongebob/thumbsup.gif)

Note: 

* Félicitation, votre container docker a xdebug
* Possibilité de faire une image custom
* Et d'installer avec pecl