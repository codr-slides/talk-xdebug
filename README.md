# Install 

```
npm install
```

# Build theme

```
npm run theme
```

# Launch

```
npm run start
```

One launched, press 'S' to open presentator view with notes.

# Config

reveal.json => https://revealjs.com/config/
reveal-md.json => https://github.com/webpro/reveal-md/blob/master/bin/help.txt

# Export PDF

Doc: https://github.com/astefanutti/decktape

```shell
OUTPUT_DIR=./dist
OUTPUT_FILENAME=slides.pdf
URL=http://localhost:3000/index.md#/
time docker run --rm -t --net=host -v $OUTPUT_DIR:/slides astefanutti/decktape --pdf-author "Guillaume Aveline" --pdf-title "Talk XDEBUG" --size "1920x1080" $URL $OUTPUT_FILENAME
```

# To do fragments list

```
* Item 1 <!-- .element: class="fragment fade-left" -->
* Item 2 <!-- .element: class="fragment fade-left" -->
```

# Fragment display order

```
* Linux <!-- .element: class="fragment" data-fragment-index="1" -->
* IDE: phpstorm / vscode <!-- .element: class="fragment" data-fragment-index="2" -->
* Docker <!-- .element: class="fragment" data-fragment-index="3" -->
```

# Snipets

```
# Formation drupal <!-- .element: class="r-fit-text" -->

[title](https://www.example.com)

<small>
<a href="https://www.linkedin.com/in/aveline-guillaume/">Guillaume Aveline</a> - xxx
</small>

Note: 
...


---

<!-- .slide: data-background="./image1.png" -->
<!-- .slide: data-background-gradient="radial-gradient(#283b95, #17b2c3)" -->
<!-- .slide: data-background-color="aquamarine" -->

# slide2

This one does!
```

# Link to slide

```
<!-- .slide: id="test" -->
# Test

---
# Another slide
[Link](#test)
```
